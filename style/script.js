let firstNumber = readNumber("Введите первое число: ");
let secondNumber = readNumber("Введите второе число: ");
let ourSign = readSing("Введите знак:+,-,*,/");
mathematical_operation(firstNumber, secondNumber, ourSign);
function readSing (message){
    while (true) {
        let sing = prompt(message).trim();

        if (typeof sing === 'string' && sing === '') {
            alert("Вы не ввели знак, попробуйте снова:");
            continue;
        }
        if (!"+-*/".includes(sing)){
            alert("Вы не ввели знак");
            continue;
        }
        return sing;
    }
}
function readNumber(message) {
    while (true) {
        let number = prompt(message).trim();

        if (typeof number === 'string' && number === '') {
            alert("Вы не ввели число, попробуйте снова:");
            continue;
        }
        if (isNaN(+number)) {
            alert("Вы ввели не число")
            continue;
        }
        return number;
    }
}
function mathematical_operation(firstNumber, secondNumber, sign) {
    switch (sign) {
        case '+':
            return console.log(`Ваше условие: ${firstNumber} + ${secondNumber}, Ваш ответ: ${+firstNumber + +secondNumber}`);
        case '-':
            return console.log(`Ваше условие: ${firstNumber} - ${secondNumber}, Ваш ответ: ${+firstNumber - +secondNumber}`);
        case '*':
            return console.log(`Ваше условие: ${firstNumber} * ${secondNumber}, Ваш ответ: ${+firstNumber * +secondNumber}`);
        case '/':
            return console.log(`Ваше условие: ${firstNumber} / ${secondNumber}, Ваш ответ: ${+firstNumber / +secondNumber}`);
        default:
    }

}

